﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace TestPowerVector
{
    class Program
    {
        private ConcurrentBag<int> _bag;

        private const int LENGTH_ARRAY = 8 * 256 * 256 * 256;
        private const int COUNT = 4;

        private static int[] iGetRndArray(int length)
        {
            var rnd = new Random();
            var tmpArray = new int[length];
            for (var i = 0; i < tmpArray.Length; i++)
            {
                tmpArray[i] = rnd.Next(0, 1000);
            }

            return tmpArray;
        }

        private static int[] iGetRndArrayVector4(int length)
        {
            var rnd = new Random();
            var tmpArray = new int[length];
            for (var i = 0; i < tmpArray.Length / 4; i += 4)
            {
                tmpArray[i] = rnd.Next(0, 1000);
                tmpArray[i + 1] = rnd.Next(0, 1000);
                tmpArray[i + 2] = rnd.Next(0, 1000);
                tmpArray[i + 3] = rnd.Next(0, 1000);
            }

            return tmpArray;
        }

        private static int[] iGetRndArrayVector8(int length)
        {
            var rnd = new Random();
            var tmpArray = new int[length];
            for (var i = 0; i < tmpArray.Length / 8; i += 8)
            {
                tmpArray[i] = rnd.Next(0, 1000);
                tmpArray[i + 1] = rnd.Next(0, 1000);
                tmpArray[i + 2] = rnd.Next(0, 1000);
                tmpArray[i + 3] = rnd.Next(0, 1000);
                tmpArray[i + 4] = rnd.Next(0, 1000);
                tmpArray[i + 5] = rnd.Next(0, 1000);
                tmpArray[i + 6] = rnd.Next(0, 1000);
                tmpArray[i + 7] = rnd.Next(0, 1000);
            }

            return tmpArray;
        }

        public static int[] iAddArrays(int[] a, int[] b)
        {
            return a.Zip(b, (x, y) => x + y).ToArray();
        }

        private static int[] iSummArrayV0(int[] A, int[] B)
        {
            if (A.Length != B.Length)
                throw new Exception("Размерности массивов должны быть одинаковы");

            var tmpArray = new int[A.Length];
            for (var i = 0; i < A.Length; i++)
            {
                tmpArray[i] = A[i] + B[i];
            }
            return tmpArray;
        }

        private static int[] iSummArrayV4(int[] A, int[] B)
        {
            var tmpArray = new int[A.Length];
            for (var i = 0; i < A.Length / 4; i += 4)
            {
                tmpArray[i] = A[i] + B[i];
                tmpArray[i + 1] = A[i + 1] + B[i + 1];
                tmpArray[i + 2] = A[i + 2] + B[i + 2];
                tmpArray[i + 3] = A[i + 3] + B[i + 3];
            }
            return tmpArray;
        }

        private static int[] iSummArrayV8(int[] A, int[] B)
        {
            var tmpArray = new int[A.Length];
            for (var i = 0; i < A.Length / 8; i += 8)
            {
                tmpArray[i] = A[i] + B[i];
                tmpArray[i + 1] = A[i + 1] + B[i + 1];
                tmpArray[i + 2] = A[i + 2] + B[i + 2];
                tmpArray[i + 3] = A[i + 3] + B[i + 3];
                tmpArray[i + 4] = A[i + 4] + B[i + 4];
                tmpArray[i + 5] = A[i + 5] + B[i + 5];
                tmpArray[i + 6] = A[i + 6] + B[i + 6];
                tmpArray[i + 7] = A[i + 7] + B[i + 7];
            }
            return tmpArray;
        }

        private static int[] iSummArrayV16(int[] A, int[] B)
        {
            var tmpArray = new int[A.Length];
            for (var i = 0; i < A.Length / 16; i += 16)
            {
                tmpArray[i] = A[i] + B[i];
                tmpArray[i + 1] = A[i + 1] + B[i + 1];
                tmpArray[i + 2] = A[i + 2] + B[i + 2];
                tmpArray[i + 3] = A[i + 3] + B[i + 3];
                tmpArray[i + 4] = A[i + 4] + B[i + 4];
                tmpArray[i + 5] = A[i + 5] + B[i + 5];
                tmpArray[i + 6] = A[i + 6] + B[i + 6];
                tmpArray[i + 7] = A[i + 7] + B[i + 7];
                tmpArray[i + 8] = A[i + 8] + B[i + 8];
                tmpArray[i + 9] = A[i + 9] + B[i + 9];
                tmpArray[i + 10] = A[i + 10] + B[i + 10];
                tmpArray[i + 11] = A[i + 11] + B[i + 11];
                tmpArray[i + 12] = A[i + 12] + B[i + 12];
                tmpArray[i + 13] = A[i + 13] + B[i + 13];
                tmpArray[i + 14] = A[i + 14] + B[i + 14];
                tmpArray[i + 15] = A[i + 15] + B[i + 15];
            }
            return tmpArray;
        }

        private static int[] iSummArrayV32(int[] A, int[] B)
        {
            var tmpArray = new int[A.Length];
            for (var i = 0; i < A.Length / 32; i += 32)
            {
                tmpArray[i] = A[i] + B[i];
                tmpArray[i + 1] = A[i + 1] + B[i + 1];
                tmpArray[i + 2] = A[i + 2] + B[i + 2];
                tmpArray[i + 3] = A[i + 3] + B[i + 3];
                tmpArray[i + 4] = A[i + 4] + B[i + 4];
                tmpArray[i + 5] = A[i + 5] + B[i + 5];
                tmpArray[i + 6] = A[i + 6] + B[i + 6];
                tmpArray[i + 7] = A[i + 7] + B[i + 7];
                tmpArray[i + 8] = A[i + 8] + B[i + 8];
                tmpArray[i + 9] = A[i + 9] + B[i + 9];
                tmpArray[i + 10] = A[i + 10] + B[i + 10];
                tmpArray[i + 11] = A[i + 11] + B[i + 11];
                tmpArray[i + 12] = A[i + 12] + B[i + 12];
                tmpArray[i + 13] = A[i + 13] + B[i + 13];
                tmpArray[i + 14] = A[i + 14] + B[i + 14];
                tmpArray[i + 15] = A[i + 15] + B[i + 15];
                tmpArray[i + 16] = A[i + 16] + B[i + 16];
                tmpArray[i + 17] = A[i + 17] + B[i + 17];
                tmpArray[i + 18] = A[i + 18] + B[i + 18];
                tmpArray[i + 19] = A[i + 19] + B[i + 19];
                tmpArray[i + 20] = A[i + 20] + B[i + 20];
                tmpArray[i + 21] = A[i + 21] + B[i + 21];
                tmpArray[i + 22] = A[i + 22] + B[i + 22];
                tmpArray[i + 23] = A[i + 23] + B[i + 23];
                tmpArray[i + 24] = A[i + 24] + B[i + 24];
                tmpArray[i + 25] = A[i + 25] + B[i + 25];
                tmpArray[i + 26] = A[i + 26] + B[i + 26];
                tmpArray[i + 27] = A[i + 27] + B[i + 27];
                tmpArray[i + 28] = A[i + 28] + B[i + 28];
                tmpArray[i + 29] = A[i + 29] + B[i + 29];
                tmpArray[i + 30] = A[i + 30] + B[i + 30];
                tmpArray[i + 31] = A[i + 31] + B[i + 31];
            }
            return tmpArray;
        }

        static void Main(string[] args)
        {
            var _sw = new Stopwatch();
          
            for (var i = 0; i < COUNT; i++)
            {
                Console.WriteLine($"i{i}");
                Console.WriteLine($"-------------------------------------------------------");

                var array0 = iGetRndArrayVector8(LENGTH_ARRAY);
                var array1 = iGetRndArrayVector8(LENGTH_ARRAY);
                
                GC.Collect(2, GCCollectionMode.Forced);
                Console.WriteLine();

                //_sw.Restart();
                //var arrayS = iAddArrays(array0, array1);
                //_sw.Stop();
                //Console.WriteLine($"sv0(zip) = {_sw.ElapsedMilliseconds} мс");

                _sw.Restart();
                var arrayS0 = iSummArrayV0(array0, array1);
                _sw.Stop();
                Console.WriteLine($"sv0 = {_sw.ElapsedMilliseconds} мс");

                _sw.Restart();
                var arrayS1 = iSummArrayV4(array0, array1);
                _sw.Stop();
                Console.WriteLine($"sv4 = {_sw.ElapsedMilliseconds} мс");

                _sw.Restart();
                var arrayS2 = iSummArrayV8(array0, array1);
                _sw.Stop();
                Console.WriteLine($"sv8 = {_sw.ElapsedMilliseconds} мс");

                _sw.Restart();
                var arrayS3 = iSummArrayV16(array0, array1);
                _sw.Stop();
                Console.WriteLine($"sv16 = {_sw.ElapsedMilliseconds} мс");

                _sw.Restart();
                var arrayS4 = iSummArrayV32(array0, array1);
                _sw.Stop();
                Console.WriteLine($"sv32 = {_sw.ElapsedMilliseconds} мс");

                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
